/**
 * Get a random hex color from a string.
 *
 * @source https://stackoverflow.com/a/16348977/11732994
 * @jsfiddle http://jsfiddle.net/o7jgdvbc/1/
 * @param salt
 */
export const getRandomColorFromString = (salt: string): string => {
  let hash = 0;
  for (let i = 0; i < salt.length; i++) {
    // eslint-disable-next-line no-bitwise
    hash = salt.charCodeAt(i) + ((hash << 5) - hash);
  }
  let colour = '#';
  for (let i = 0; i < 3; i++) {
    // eslint-disable-next-line no-bitwise
    const value = (hash >> (i * 8)) & 0xff;
    colour += ('00' + value.toString(16)).substr(-2);
  }
  return colour;
};
