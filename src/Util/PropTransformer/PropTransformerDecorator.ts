import 'reflect-metadata';

const propTransformerDecoratorMetadataKey = Symbol('propTransformer');

export function propTransformerDecorator(formatString: string) {
  return Reflect.metadata(propTransformerDecoratorMetadataKey, formatString);
}

export function getPropTransformerDecorator(target: any, propertyKey: string) {
  return Reflect.getMetadata(
    propTransformerDecoratorMetadataKey,
    target,
    propertyKey
  );
}
