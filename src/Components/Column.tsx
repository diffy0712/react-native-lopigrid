import AbstractComponent, {
  AbstractComponentProps,
  AbstractComponentState,
} from './AbstractComponent';
import _ from 'lodash';
import { gridColumns } from '../Config';

interface ColumnProps extends AbstractComponentProps {}

interface ColumnState extends AbstractComponentState {}

export default class Column extends AbstractComponent<
  ColumnProps,
  ColumnState
> {
  protected getIncomingPropsStyle() {
    const { size } = this.props;
    const sizeForScreen = !size
      ? gridColumns
      : typeof size === 'number'
      ? size
      : this.getSizeForScreen(size, 'width');
    const columnSize = (sizeForScreen * 100) / gridColumns;
    const style = _.merge(
      {
        flexBasis: `${columnSize}%`,
        maxWidth: `${columnSize}%`,
        display: size === 0 ? 'none' : 'flex',
      },
      super.getIncomingPropsStyle()
    );

    return style;
  }
}
