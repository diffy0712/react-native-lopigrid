import React from 'react';
import AbstractComponent, {
  AbstractComponentProps,
  AbstractComponentState,
} from './AbstractComponent';
import Row from './Row';
import _ from 'lodash';

export interface ContainerProps extends AbstractComponentProps {}

export interface ContainerState extends AbstractComponentState {}

export default class Container extends AbstractComponent<
  ContainerProps,
  ContainerState
> {
  protected getIncomingPropsStyle() {
    const style = _.merge(
      {
        flex: 1,
        flexDirection: this.ifRow() ? 'column' : 'row',
      },
      super.getIncomingPropsStyle()
    );

    return style;
  }

  protected ifRow() {
    let row = false;
    React.Children.forEach(this.props.children, (child: any): void => {
      if (child && child.type === Row) {
        row = true;
      }
    });
    return row;
  }
}
