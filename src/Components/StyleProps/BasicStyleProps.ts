// import { propTransformerDecorator } from '../../Util/PropTransformer/PropTransformerDecorator';

/**
 * Basic style props to apply to your view
 */
export class BasicStylePropsClass {
  /**
   * Add background color to the Component
   */
  bg?: string = undefined;

  /**
   * Add border to the Component
   */
  border?: string = undefined;

  /**
   * BorderWidth
   */
  // regisztrálja a prop key-ét a hozzá tartozó default value-val ha lehet
  // és meghívja majd hozzá a transformert ami átalakítja style proppá
  // pl: {
  //    key: p,
  //    param: StylePropTransformer,
  //    value: // default value if any otherwise undefined.
  // }
  borderW?: number = undefined;

  /**
   * Add padding to the Component
   */
  // @propTransformerDecorator('asd') // nem lenne ez túl heavy? :/
  p?: number | number[] = 0;

  /**
   * Add margin to the Component
   */
  m?: number | number[] = 0;

  /**
   * Height
   */
  h?: number | string = undefined;

  /**
   * Width
   */
  w?: number | string = undefined;
}

export interface BasicStyleProps extends BasicStylePropsClass {}

export const BasicStylePropsMapping = {
  bg: 'backgroundColor',
  border: 'borderColor',
  borderW: 'borderWidth',
  p: 'padding',
  m: 'margin',
  h: 'height',
  w: 'width',
};
