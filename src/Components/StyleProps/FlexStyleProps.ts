import { FlexAlignType } from 'react-native';

/**
 * Flex style props to apply to your view
 */
export class FlexStylePropsClass {
  alignContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'stretch'
    | 'space-between'
    | 'space-around' = 'flex-start';
  alignItems?: FlexAlignType = 'flex-start';
  flexWrap?: 'wrap' | 'nowrap' | 'wrap-reverse' = 'wrap';
  justifyContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly' = 'flex-start';
  overflow?: 'visible' | 'hidden' | 'scroll' = 'scroll';
  flex?: number = 1;
  flexBasis?: number | string = '100%';
  flexDirection?: 'row' | 'column' | 'row-reverse' | 'column-reverse' =
    'column';
  flexGrow?: number = 1;
  flexShrink?: number = 1;
}

export interface FlexStyleProps extends FlexStylePropsClass {}

export const FlexStylePropsMapping = {
  alignContent: 'alignContent',
  alignItems: 'alignItems',
  flexWrap: 'flexWrap',
  justifyContent: 'justifyContent',
  overflow: 'overflow',
};
