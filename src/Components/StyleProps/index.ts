import _ from 'lodash';
import { BasicStylePropsMapping, BasicStyleProps } from './BasicStyleProps';
import { FlexStylePropsMapping, FlexStyleProps } from './FlexStyleProps';

export interface StyleProps extends BasicStyleProps, FlexStyleProps {}

export const StylePropsMappings = _.merge(
  BasicStylePropsMapping,
  FlexStylePropsMapping
);
