import React, { Component, ReactElement } from 'react';
import {
  View,
  TouchableOpacity,
  ViewProps,
  StyleSheet,
  ViewStyle,
  GestureResponderEvent,
  Dimensions,
} from 'react-native';
import _ from 'lodash';
import { StyleProps, StylePropsMappings } from './StyleProps';
import { screenResolutions } from '../Config';
import { getRandomColorFromString } from '../Util/color';
import { observer } from 'mobx-react';

import { observable, makeObservable, action } from 'mobx';

class DimensionsStore {
  width?: number;
  height?: number;

  constructor() {
    this.reset();
    makeObservable(this, {
      width: observable,
      height: observable,
      reset: action,
    });
  }

  reset() {
    (this.width = Dimensions.get('window').width),
      (this.height = Dimensions.get('window').height);
  }
}

const dimensionsStore = new DimensionsStore();

export interface AbstractComponentProps extends ViewProps, StyleProps {
  /**
   * The View does not have onPress so add one to make our lifes easier
   */
  onPress?: (event: GestureResponderEvent) => void;

  /**
   * Given as an array:
   *  - [xs, sm, md, lg, xl]
   */
  size?: number | number[];

  debug?: boolean;
}

export interface AbstractComponentState {}

abstract class AbstractComponent<
  P extends AbstractComponentProps,
  T extends AbstractComponentState
> extends Component<P, T> {
  static dimensionEventListener: any;
  static defaultProps: AbstractComponentProps = {
    debug: false,
  };
  static instanceCount = 0;
  protected key = 33333 * AbstractComponent.instanceCount;

  public _root?: View | null;

  public state = {
    dimensions: dimensionsStore,
  };

  constructor(props: P) {
    super(props);
    AbstractComponent.instanceCount++;

    if (typeof AbstractComponent.dimensionEventListener !== undefined) {
      AbstractComponent.dimensionEventListener = Dimensions.addEventListener(
        'change',
        () => {
          this.state.dimensions.reset();
        }
      );
    }
  }

  public render(): ReactElement {
    const view = this.renderView();

    return this.props.onPress ? (
      <TouchableOpacity onPress={this.props.onPress}>{view}</TouchableOpacity>
    ) : (
      view
    );
  }

  public renderView(): ReactElement {
    const { children } = this.props;
    return (
      <View
        ref={(component) => (this._root = component)}
        children={children}
        {...this.computeProps()}
      />
    );
  }

  public setNativeProps(nativeProps: any) {
    this._root?.setNativeProps(nativeProps);
  }

  protected getSizeForScreen(
    size: number[],
    dimension: 'width' | 'height'
  ): number {
    const windowDimensionSize = Dimensions.get('window')[dimension];
    let sizeKey: number = 0;

    _.forEach(screenResolutions[dimension], (_size: number) => {
      if (windowDimensionSize > _size) {
        sizeKey += size[sizeKey + 1] ? 1 : 0;
      }
    });

    return size[sizeKey];
  }

  /**
   * The component can define default props, which might
   * be overridden
   */
  protected getIncomingPropsStyle() {
    const style: ViewStyle = {};

    _.forEach(StylePropsMappings, (StylePropsMapping, index) => {
      if (!this.props[index]) {
        return;
      }
      if (['p', 'm'].includes(index)) {
        const stylePropValues = this.getSideStylesFromValueArray(
          this.props[index]
        );
        style[StylePropsMapping + 'Top'] = stylePropValues.top;
        style[StylePropsMapping + 'Right'] = stylePropValues.right;
        style[StylePropsMapping + 'Bottom'] = stylePropValues.bottom;
        style[StylePropsMapping + 'Left'] = stylePropValues.left;
        return;
      }

      style[StylePropsMapping] = this.props[index];
    });

    if (this.props.debug) {
      style.backgroundColor = getRandomColorFromString(this.key.toString());
    }

    return style;
  }

  private getSideStylesFromValueArray(
    values: number[]
  ): {
    top: number;
    right: number;
    bottom: number;
    left: number;
  } {
    return {
      top: values[0],
      right: typeof values[1] !== 'undefined' ? values[1] : values[0],
      bottom: typeof values[2] !== 'undefined' ? values[2] : values[0],
      left:
        typeof values[3] !== 'undefined'
          ? values[3]
          : typeof values[1] !== 'undefined'
          ? values[1]
          : values[0],
    };
  }

  /**
   * Fetch and merge the styling from
   * the flattened `style` props of the component
   * and the styles coming as props.
   */
  private computeProps() {
    const computedProps: ViewProps = _.clone(this.props);
    computedProps.style = _.merge(
      this.getIncomingPropsStyle(),
      this.getIncomingFlattenedStyles()
    );
    computedProps.maxWidth = this.state.dimensions.width;

    return computedProps;
  }

  /**
   * Flattens the style props value so later it can be merged
   *
   * @param incomingPropsStyle  could be anything coming from a prop
   *                            in react-native it can be a `ViewStyle|StylesheetID` or ViewStyle|StylesheetID`[]
   */
  private getIncomingFlattenedStyles() {
    const incomingPropsStyle = _.clone(this.props.style || {});
    if (!Array.isArray(incomingPropsStyle)) {
      return this.getFlattenedStyle(incomingPropsStyle);
    }

    let computedPropsStyle = {};

    _.forEach(incomingPropsStyle, (style) => {
      _.merge(computedPropsStyle, this.getFlattenedStyle(style));
    });

    return computedPropsStyle;
  }

  /**
   * @param style - Might be a number of a Stylesheet ID,
   *                which could be looked up by StyleSheet.flatten.
   *              - In other cases just return it back for now!
   */
  private getFlattenedStyle(style: number | any) {
    if (typeof style !== 'number') {
      return style;
    }

    /**
     * Flattens an array of style objects, into one aggregated style object.
     * Alternatively, this method can be used to lookup IDs.
     */
    return StyleSheet.flatten(style);
  }
}

export default observer(AbstractComponent);
