import AbstractComponent, {
  AbstractComponentProps,
  AbstractComponentState,
} from './AbstractComponent';
import _ from 'lodash';
import { gridRows } from '../Config';

interface RowProps extends AbstractComponentProps {}

interface RowState extends AbstractComponentState {}

export default class Row extends AbstractComponent<RowProps, RowState> {
  protected getIncomingPropsStyle() {
    const { size } = this.props;
    const sizeForScreen = !size
      ? gridRows
      : typeof size === 'number'
      ? size
      : this.getSizeForScreen(size, 'height');
    const rowSize = (sizeForScreen * 100) / gridRows;
    const style = _.merge(
      {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        alignContent: 'flex-start',
        flexBasis: `${rowSize}%`,
        maxHeight: `${rowSize}%`,
        display: size === 0 ? 'none' : 'flex',
      },
      super.getIncomingPropsStyle()
    );

    return style;
  }
}
