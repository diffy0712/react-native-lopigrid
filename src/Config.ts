export let gridColumns: number = 12;
export let gridRows: number = 12;

export const setGridColumns = (cols: number): void => {
  gridColumns = cols;
};

export const setGridRows = (rows: number): void => {
  gridRows = rows;
};

export let screenResolutions: ScreenResolutions = {
  width: {
    xs: 400,
    sm: 500,
    md: 600,
    lg: 700,
    xl: 800,
  },
  height: {
    xs: 300,
    sm: 500,
    md: 700,
    lg: 900,
    xl: 1200,
  },
};

export const setScreenResolutions = (
  _screenResolutions: ScreenResolutions
): void => {
  screenResolutions = _screenResolutions;
};

export interface ScreenResolutions {
  width: {
    [key: string]: number;
  };
  height: {
    [key: string]: number;
  };
}
