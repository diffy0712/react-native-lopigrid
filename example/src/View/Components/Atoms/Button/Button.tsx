import { memo } from 'react';
import styled from 'styled-components/native';

// Component
const Button = styled.TouchableOpacity`
  background-color: #368db8;
  padding: 10px;
  border-radius: 20px;
`;

export default memo(Button);
