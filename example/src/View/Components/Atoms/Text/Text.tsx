import styled from 'styled-components/native';

const Text = styled.Text`
  font-size: 14px;
  font-weight: 600;
  color: #000000;
  text-align: center;
`;

export default Text;
