import styled from 'styled-components/native';
import Text from './Text';

const Title = styled(Text)`
  font-size: 20px;
  font-weight: 500;
`;

export default Title;
