import * as React from 'react';
import { Column, Row, Container } from 'react-native-lopigrid';
import { Text } from 'react-native';

export const Card = (props: any) => {
  const { id, name, ...rest } = props;
  return (
    <Container {...rest}>
      <Row bg="pink" p={10}>
        <Column justifyContent="center" alignItems="flex-start">
          <Text>Id: #{id}</Text>
          <Text>Name: {name}</Text>
        </Column>
        <Column justifyContent="center" alignItems="flex-end">
          <Text>
            Some loooong text. Some loooong text. Some loooong text. {name} {name}
          </Text>
        </Column>
      </Row>
    </Container>
  );
};
