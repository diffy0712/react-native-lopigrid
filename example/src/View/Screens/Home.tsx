import React from 'react';
import { Container, Row, Column } from 'react-native-lopigrid';
import styled from 'styled-components/native';
import AbstractScreen, {
  AbstractScreenProps,
  AbstractScreenState,
} from './AbstractScreen';
import { Text, Title, Button } from '../Components/Atoms';
import Logo from '../Components/Atoms/Logo/Logo';
import { ScreensEnum } from './Router';

export interface HomeScreenProps extends AbstractScreenProps {}

export interface HomeScreenState extends AbstractScreenState {}

class HomeScreen extends AbstractScreen<HomeScreenProps, HomeScreenState> {
  render() {
    return (
      <Container bg="papayawhip" justifyContent="center">
        <Column alignItems="stretch" justifyContent="center">
          <Row size={5} justifyContent="center" p={[10]} debug>
            <Title>React Native with 💅 Styled Components</Title>
          </Row>
          <Row size={2} justifyContent="center" alignContent="center">
            <Logo />
          </Row>
          <Row size={2} justifyContent="center" p={20} debug>
            <Text>Some description...</Text>
          </Row>
          <Row size={3} justifyContent="center" alignContent="flex-end">
            <Column>
              <FooterButton
                onPress={() =>
                  this.props.navigation.navigate(ScreensEnum.Foods)
                }
              >
                <Text>Login</Text>
              </FooterButton>
              <FooterButton
                onPress={() =>
                  this.props.navigation.navigate(ScreensEnum.NotFound)
                }
              >
                <Text>Register</Text>
              </FooterButton>
            </Column>
          </Row>
        </Column>
      </Container>
    );
  }
}

export default HomeScreen;

const FooterButton = styled(Button)`
  margin-bottom: 10px;
  margin-right: 10px;
  margin-left: 10px;
`;
