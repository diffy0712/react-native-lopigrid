import React from 'react';
import { View, Text } from 'react-native';
import AbstractScreen, {
  AbstractScreenProps,
  AbstractScreenState,
} from './AbstractScreen';

export interface NotFoundScreenProps extends AbstractScreenProps {}

export interface NotFoundScreenState extends AbstractScreenState {}

class NotFoundScreen extends AbstractScreen<
  NotFoundScreenProps,
  NotFoundScreenState
> {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>404 - Screen Not Found</Text>
      </View>
    );
  }
}

export default NotFoundScreen;
