import HomeScreen from './Home';
import FoodsScreen from './Foods';
import NotFoundScreen from './NotFoundScreen';

export { HomeScreen, FoodsScreen, NotFoundScreen };
