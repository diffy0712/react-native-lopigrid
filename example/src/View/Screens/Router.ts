import HomeScreen from './Home';
import { FoodsScreen } from '.';
import NotFoundScreen from './NotFoundScreen';

export enum ScreensEnum {
  Home = 'Home',
  Foods = 'Foods',
  NotFound = 'NotFound',
}

export const Routes = {
  [ScreensEnum.Home]: HomeScreen,
  [ScreensEnum.Foods]: FoodsScreen,
  [ScreensEnum.NotFound]: NotFoundScreen,
};
