import React from 'react';
import { View, Text } from 'react-native';
import AbstractScreen, {
  AbstractScreenProps,
  AbstractScreenState,
} from './AbstractScreen';

export interface HomeScreenProps extends AbstractScreenProps {}

export interface HomeScreenState extends AbstractScreenState {}

class FoodScreen extends AbstractScreen<HomeScreenProps, HomeScreenState> {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Products Screen</Text>
      </View>
    );
  }
}

export default FoodScreen;
