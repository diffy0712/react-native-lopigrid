import { Component } from 'react';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';

export interface AbstractScreenProps {
  navigation: StackNavigationHelpers;
}

export interface AbstractScreenState {}

abstract class AbstractScreen<
  P extends AbstractScreenProps,
  T extends AbstractScreenState
> extends Component<P, T> {}

export default AbstractScreen;
