import Food from '../Model/Food';
import foods from '../assets/fixtures/foods.json';

export const fetchFoods = (): Food[] => {
  return foods;
};
