import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ScreensEnum, Routes } from './View/Screens/Router';

const Stack = createStackNavigator();

export default () => (
  <NavigationContainer>
    <Stack.Navigator initialRouteName={ScreensEnum.Home} headerMode="none">
      {Object.keys(Routes).map((value) => (
        <Stack.Screen name={value} component={Routes[value]} />
      ))}
    </Stack.Navigator>
  </NavigationContainer>
);
