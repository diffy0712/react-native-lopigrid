export default interface Food {
  id: string;
  name: string;
  slug: string;
  image: string;
  categories: string[];
}
