# react-native-lopigrid

A (not responsive) grid system for react-native.

> **IMPORTANT:** This package was never finished and not maintained anymore. Should not be used in production.

## Installation

```sh
yarn add react-native-lopigrid
```

## Usage

```js
import Lopigrid from "react-native-lopigrid";

<Container>
  <Column>
    <Row size={40} style={{ backgroundColor: 'green' }} />
    <Row size={20} style={[styles.text, null, { backgroundColor: 'red' }]}/>
    <Row size={25} />
    <Row size={15} />
  </Column>
</Container>
```

### StyleProps
|Prop|default|type|style|
|---|---|---|---|
|p|`undefined`|`number or number[]`|`padding`|
|m|`undefined`|`number or number[]`|`margin`|
|h|`undefined`|`number`|`height`|
|w|`undefined`|`number`|`width`|
|bg|`undefined`|`string`|`background-color`|
|border|`undefined`|`string`| `border-color`|
|borderWidth|`undefined`|`number`|`border-width`|
|debug|`false`|`boolean`|`random background-color`|

### FlexProps
The following flex props are available:
 - alignContent
 - alignItems
 - flexWrap
 - justifyContent
 - overflow
 - flex
 - flexBasis
 - flexDirection
 - flexGrow
 - flexShrink

## Example
There is an example food order application for restaurants using [Ghulam Rasool's](https://dribbble.com/ghulaam-rasool) designs.
 - [Desktop and tablet](https://dribbble.com/shots/12998351-Food-Mobile-APP-Landing-UX-UI-Design/attachments/4599331?mode=media)
 - [Mobile](https://dribbble.com/shots/12473055/attachments/4083931?mode=media)

Free stock images used:
  - foods:
    - [carbonara-in-gray-bowl](https://www.pexels.com/photo/carbonara-in-gray-bowl-1030947/)
    - [green-leafy-vegetable-dish-in-gray-steel-bowl-with-fork](https://www.pexels.com/photo/green-leafy-vegetable-dish-in-gray-steel-bowl-with-fork-842571/)
    - [vegetable-sandwich-on-plate](https://www.pexels.com/photo/vegetable-sandwich-on-plate-1095550/)
    - [basil-leaves-and-avocado-on-sliced-bread-on-white-ceramic-plate](https://www.pexels.com/photo/basil-leaves-and-avocado-on-sliced-bread-on-white-ceramic-plate-1351238/)
    - [cooked-food-with-meats-and-vegetables-inside-white-bowl](https://www.pexels.com/photo/cooked-food-with-meats-and-vegetables-inside-white-bowl-1070053/)
    - [flat-lay-photography-of-vegetable-salad-on-plate](https://www.pexels.com/photo/flat-lay-photography-of-vegetable-salad-on-plate-1640777/)
  - other:

## Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

### [Unreleased]
#### Idea
- [ ] add background image as bg, from url or local file
- [ ] add tests
- [ ] set up ci/cd to run tests?
- [ ] Generate each component with debug prop an id, so we can generate a custom color for them and propagate the debug prop down the tree. this would give nice visualization on our grid just by applying one props on the top element.
      Should be a unique color by props or something, to always get the same color generated for a given component.
#### Fix
- [ ] borderB adds extra width and height on component

### [0.3.1] - 2020-12-23
#### Fixed
- unable to add p and m as a number (only working with an array)
- a bit updated documentation

### [0.2.3] - 2020-10-20
#### Fixed
- Apparenlty react-native fails to refresh the component's flexBasis after orientation change so a simple store was added to trigger a rerender on an orientation change. Not the best solution but works for now.

### [0.1.3] - 2020-06-20
#### Added
- Debug props on Components so they get a random background color to easier debug.
#### Changed
- `Grid` Component renamed to `Container`

#### Fixed
- Fix converting `m` and `p` props as array to style

### [0.1.3] - 2020-06-19
#### Added
- Margin & Padding as array

#### Fiexed
- Resonsive problems
### [0.1.2] - 2020-06-18
#### Fixed
- fixed `AbstractComponent::computeProps` to fix style props not applying problems.

### [0.1.1] - 2020-06-14
#### Added
- Grid Component to serve as a main View to contain Columns and Rows
- Column Component
- Row Component
- Responsive sizes



## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
